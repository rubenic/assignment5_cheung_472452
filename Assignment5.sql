-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 23, 2017 at 08:00 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Assignment5`
--

-- --------------------------------------------------------

--
-- Table structure for table `Club`
--

CREATE TABLE `Club` (
  `id` varchar(40) NOT NULL,
  `clubName` varchar(40) NOT NULL,
  `city` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Club`
--

INSERT INTO `Club` (`id`, `clubName`, `city`) VALUES
('asker-ski', 'Asker skiklubb', 'Asker'),
('lhmr-ski', 'Lillehammer Skiklub', 'Lillehammer'),
('skiklubben', 'Trondhjems skiklub', 'Trondheim'),
('vindil', 'Vind Idrettslag', 'Gjøvik');

-- --------------------------------------------------------

--
-- Table structure for table `ClubCity`
--

CREATE TABLE `ClubCity` (
  `city` varchar(40) NOT NULL,
  `county` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ClubCity`
--

INSERT INTO `ClubCity` (`city`, `county`) VALUES
('Asker', 'Akershus'),
('Gjøvik', 'Oppland'),
('Lillehammer', 'Oppland'),
('Trondheim', 'Sør-Trøndelag');

-- --------------------------------------------------------

--
-- Table structure for table `Logs`
--

CREATE TABLE `Logs` (
  `totalDistance` int(11) NOT NULL,
  `SuserName` varchar(40) NOT NULL,
  `year` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Logs`
--

INSERT INTO `Logs` (`totalDistance`, `SuserName`, `year`) VALUES
(3, '?hal_?mos', 2016),
(2, '?jan_tang', 2015),
(4, '?jan_tang', 2016),
(1237, '?rut_?mos', 2016),
(368, '?rut_nord', 2016),
(23, 'ande_andr', 2015),
(55, 'ande_andr', 2016),
(942, 'ande_rønn', 2015),
(440, 'andr_stee', 2015),
(379, 'andr_stee', 2016),
(3, 'anna_næss', 2015),
(3, 'anna_næss', 2016),
(32, 'arne_anto', 2015),
(99, 'arne_anto', 2016),
(1, 'arne_inge', 2015),
(2, 'arne_inge', 2016),
(961, 'astr_amun', 2015),
(761, 'astr_amun', 2016),
(1, 'astr_sven', 2015),
(3, 'astr_sven', 2016),
(19, 'bent_håla', 2015),
(62, 'bent_håla', 2016),
(125, 'bent_svee', 2015),
(448, 'beri_hans', 2015),
(374, 'beri_hans', 2016),
(121, 'bjør_aase', 2015),
(116, 'bjør_aase', 2016),
(47, 'bjør_ali', 2015),
(47, 'bjør_ali', 2016),
(33, 'bjør_rønn', 2015),
(56, 'bjør_rønn', 2016),
(460, 'bjør_sand', 2015),
(449, 'bjør_sand', 2016),
(243, 'bror_?mos', 2016),
(202, 'bror_kals', 2016),
(1, 'cami_erik', 2015),
(1, 'cami_erik', 2016),
(33, 'dani_hamm', 2015),
(61, 'dani_hamm', 2016),
(31, 'eina_nygå', 2015),
(68, 'eina_nygå', 2016),
(341, 'elis_ruud', 2015),
(368, 'elis_ruud', 2016),
(12, 'elle_wiik', 2015),
(35, 'elle_wiik', 2016),
(122, 'erik_haal', 2015),
(143, 'erik_haal', 2016),
(1, 'erik_lien', 2015),
(581, 'erik_pete', 2015),
(519, 'espe_egel', 2015),
(556, 'espe_egel', 2016),
(1, 'espe_haal', 2015),
(2, 'espe_haal', 2016),
(28, 'eva_kvam', 2015),
(89, 'eva_kvam', 2016),
(113, 'fred_lien', 2015),
(122, 'fred_lien', 2016),
(1, 'frod_mads', 2015),
(2, 'frod_mads', 2016),
(237, 'frod_rønn', 2015),
(69, 'geir_birk', 2015),
(71, 'geir_birk', 2016),
(891, 'geir_herm', 2015),
(789, 'geir_herm', 2016),
(173, 'gerd_svee', 2015),
(196, 'gerd_svee', 2016),
(2, 'gunn_berg', 2015),
(2, 'gunn_berg', 2016),
(17, 'guri_nord', 2016),
(14, 'hann_stei', 2016),
(240, 'hans_foss', 2015),
(276, 'hans_foss', 2016),
(3, 'hans_løke', 2015),
(1, 'hans_løke', 2016),
(7, 'hara_bakk', 2015),
(16, 'hara_bakk', 2016),
(3, 'heid_dani', 2015),
(3, 'heid_dani', 2016),
(27, 'helg_brei', 2015),
(74, 'helg_brei', 2016),
(9, 'helg_toll', 2015),
(799, 'henr_bern', 2015),
(2, 'henr_dale', 2015),
(2, 'henr_dale', 2016),
(1, 'henr_lore', 2015),
(1, 'henr_lore', 2016),
(2, 'hild_hass', 2015),
(1, 'hild_hass', 2016),
(778, 'håko_jens', 2015),
(804, 'håko_jens', 2016),
(101, 'idar_kals', 2016),
(1308, 'idar_kals1', 2016),
(666, 'ida_mykl', 2015),
(614, 'ida_mykl', 2016),
(3, 'inge_simo', 2015),
(2, 'inge_simo', 2016),
(194, 'inge_thor', 2015),
(220, 'inge_thor', 2016),
(294, 'ingr_edva', 2015),
(309, 'ingr_edva', 2016),
(20, 'juli_ande', 2015),
(34, 'juli_ande', 2016),
(261, 'kari_thor', 2015),
(233, 'kari_thor', 2016),
(1, 'kjel_fjel', 2015),
(2, 'kjel_fjel', 2016),
(2, 'knut_bye', 2015),
(586, 'kris_even', 2015),
(4, 'kris_hass', 2015),
(11, 'kris_hass', 2016),
(391, 'kris_hass1', 2015),
(334, 'kris_hass1', 2016),
(578, 'lind_lore', 2015),
(551, 'lind_lore', 2016),
(178, 'liv_khan', 2015),
(183, 'liv_khan', 2016),
(200, 'magn_sand', 2015),
(166, 'magn_sand', 2016),
(362, 'mari_bye', 2015),
(576, 'mari_dahl', 2015),
(492, 'mari_dahl', 2016),
(18, 'mari_eile', 2015),
(18, 'mari_eile', 2016),
(41, 'mari_stra', 2015),
(35, 'mari_stra', 2016),
(63, 'mart_halv', 2015),
(50, 'mart_halv', 2016),
(7, 'mona_lie', 2015),
(12, 'mona_lie', 2016),
(2, 'mort_iver', 2015),
(4, 'mort_iver', 2016),
(36, 'nils_bakk', 2015),
(93, 'nils_bakk', 2016),
(4, 'nils_knud', 2015),
(2, 'nils_knud', 2016),
(352, 'odd_moha', 2015),
(17, 'olav_bråt', 2015),
(19, 'olav_bråt', 2016),
(2, 'olav_eike', 2015),
(2, 'olav_eike', 2016),
(1, 'olav_hell', 2015),
(408, 'olav_lien', 2015),
(423, 'olav_lien', 2016),
(311, 'ole_borg', 2015),
(314, 'ole_borg', 2016),
(2, 'reid_hamr', 2015),
(3, 'reid_hamr', 2016),
(749, 'rolf_wiik', 2015),
(632, 'rolf_wiik', 2016),
(228, 'rune_haga', 2015),
(248, 'rune_haga', 2016),
(5, 'sara_okst', 2016),
(1, 'silj_mykl', 2015),
(2, 'silj_mykl', 2016),
(1, 'sive_nord', 2016),
(2, 'solv_solb', 2015),
(1, 'solv_solb', 2016),
(8, 'stia_andr', 2015),
(9, 'stia_andr', 2016),
(412, 'stia_haug', 2015),
(443, 'stia_haug', 2016),
(62, 'stia_henr', 2015),
(49, 'stia_henr', 2016),
(119, 'terj_mort', 2015),
(95, 'terj_mort', 2016),
(15, 'thom_inge', 2015),
(26, 'thom_inge', 2016),
(1, 'tom_bråt', 2015),
(1, 'tom_bråt', 2016),
(176, 'tom_bøe', 2015),
(194, 'tom_bøe', 2016),
(18, 'tom_jako', 2015),
(33, 'tom_jako', 2016),
(375, 'tore_gulb', 2015),
(342, 'tore_gulb', 2016),
(1156, 'tore_svee', 2015),
(408, 'tor_dale', 2015),
(321, 'tove_moe', 2015),
(352, 'tove_moe', 2016),
(22, 'trin_kals', 2016),
(3, 'tron_kris', 2015),
(5, 'tron_kris', 2016),
(8, 'tron_moen', 2015),
(17, 'tron_moen', 2016),
(2, 'øyst_aase', 2015),
(1, 'øyst_aase', 2016),
(13, 'øyst_lore', 2015),
(47, 'øyst_lore', 2016),
(831, 'øyst_sæth', 2015),
(631, 'øyst_sæth', 2016),
(950, 'øyvi_hell', 2015),
(869, 'øyvi_hell', 2016),
(3, 'øyvi_jens', 2015),
(2, 'øyvi_jens', 2016),
(18, 'øyvi_kvam', 2015),
(20, 'øyvi_vike', 2015),
(52, 'øyvi_vike', 2016);

-- --------------------------------------------------------

--
-- Table structure for table `Season`
--

CREATE TABLE `Season` (
  `fallYear` year(4) NOT NULL,
  `SuserName` varchar(40) NOT NULL,
  `Cid` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Season`
--

INSERT INTO `Season` (`fallYear`, `SuserName`, `Cid`) VALUES
(2016, '?hal_?mos', 'asker-ski'),
(2015, 'andr_stee', 'asker-ski'),
(2016, 'andr_stee', 'asker-ski'),
(2015, 'bent_håla', 'asker-ski'),
(2015, 'bent_svee', 'asker-ski'),
(2015, 'beri_hans', 'asker-ski'),
(2016, 'beri_hans', 'asker-ski'),
(2015, 'bjør_aase', 'asker-ski'),
(2016, 'bjør_aase', 'asker-ski'),
(2015, 'bjør_ali', 'asker-ski'),
(2016, 'bjør_ali', 'asker-ski'),
(2015, 'elis_ruud', 'asker-ski'),
(2015, 'fred_lien', 'asker-ski'),
(2016, 'fred_lien', 'asker-ski'),
(2015, 'geir_herm', 'asker-ski'),
(2015, 'hans_foss', 'asker-ski'),
(2015, 'heid_dani', 'asker-ski'),
(2016, 'heid_dani', 'asker-ski'),
(2015, 'henr_bern', 'asker-ski'),
(2015, 'inge_simo', 'asker-ski'),
(2016, 'inge_simo', 'asker-ski'),
(2015, 'kjel_fjel', 'asker-ski'),
(2015, 'liv_khan', 'asker-ski'),
(2016, 'liv_khan', 'asker-ski'),
(2015, 'magn_sand', 'asker-ski'),
(2016, 'magn_sand', 'asker-ski'),
(2015, 'olav_lien', 'asker-ski'),
(2016, 'olav_lien', 'asker-ski'),
(2015, 'rune_haga', 'asker-ski'),
(2016, 'rune_haga', 'asker-ski'),
(2016, 'sara_okst', 'asker-ski'),
(2015, 'silj_mykl', 'asker-ski'),
(2016, 'silj_mykl', 'asker-ski'),
(2016, 'solv_solb', 'asker-ski'),
(2015, 'tom_jako', 'asker-ski'),
(2015, 'tove_moe', 'asker-ski'),
(2016, 'tove_moe', 'asker-ski'),
(2015, 'øyvi_hell', 'asker-ski'),
(2016, 'øyvi_hell', 'asker-ski'),
(2015, 'øyvi_kvam', 'asker-ski'),
(2015, 'øyvi_vike', 'asker-ski'),
(2016, 'øyvi_vike', 'asker-ski'),
(2015, 'ande_rønn', 'lhmr-ski'),
(2015, 'astr_amun', 'lhmr-ski'),
(2016, 'astr_amun', 'lhmr-ski'),
(2015, 'bjør_rønn', 'lhmr-ski'),
(2016, 'bjør_rønn', 'lhmr-ski'),
(2015, 'bjør_sand', 'lhmr-ski'),
(2016, 'bjør_sand', 'lhmr-ski'),
(2015, 'dani_hamm', 'lhmr-ski'),
(2016, 'dani_hamm', 'lhmr-ski'),
(2015, 'eina_nygå', 'lhmr-ski'),
(2015, 'elle_wiik', 'lhmr-ski'),
(2016, 'elle_wiik', 'lhmr-ski'),
(2015, 'erik_haal', 'lhmr-ski'),
(2016, 'erik_haal', 'lhmr-ski'),
(2015, 'espe_haal', 'lhmr-ski'),
(2016, 'espe_haal', 'lhmr-ski'),
(2015, 'frod_rønn', 'lhmr-ski'),
(2015, 'gerd_svee', 'lhmr-ski'),
(2016, 'gerd_svee', 'lhmr-ski'),
(2016, 'hann_stei', 'lhmr-ski'),
(2016, 'hans_foss', 'lhmr-ski'),
(2015, 'hara_bakk', 'lhmr-ski'),
(2016, 'hara_bakk', 'lhmr-ski'),
(2015, 'hild_hass', 'lhmr-ski'),
(2016, 'hild_hass', 'lhmr-ski'),
(2015, 'håko_jens', 'lhmr-ski'),
(2016, 'håko_jens', 'lhmr-ski'),
(2015, 'knut_bye', 'lhmr-ski'),
(2016, 'kris_hass1', 'lhmr-ski'),
(2015, 'lind_lore', 'lhmr-ski'),
(2016, 'lind_lore', 'lhmr-ski'),
(2015, 'mari_dahl', 'lhmr-ski'),
(2016, 'mari_dahl', 'lhmr-ski'),
(2015, 'mari_eile', 'lhmr-ski'),
(2016, 'mari_eile', 'lhmr-ski'),
(2015, 'nils_bakk', 'lhmr-ski'),
(2016, 'nils_bakk', 'lhmr-ski'),
(2015, 'olav_eike', 'lhmr-ski'),
(2016, 'olav_eike', 'lhmr-ski'),
(2015, 'ole_borg', 'lhmr-ski'),
(2016, 'ole_borg', 'lhmr-ski'),
(2015, 'tore_gulb', 'lhmr-ski'),
(2016, 'tore_gulb', 'lhmr-ski'),
(2016, 'trin_kals', 'lhmr-ski'),
(2016, '?rut_nord', 'skiklubben'),
(2015, 'ande_andr', 'skiklubben'),
(2016, 'ande_andr', 'skiklubben'),
(2015, 'anna_næss', 'skiklubben'),
(2016, 'anna_næss', 'skiklubben'),
(2015, 'arne_anto', 'skiklubben'),
(2016, 'arne_anto', 'skiklubben'),
(2015, 'arne_inge', 'skiklubben'),
(2016, 'arne_inge', 'skiklubben'),
(2015, 'astr_sven', 'skiklubben'),
(2016, 'astr_sven', 'skiklubben'),
(2016, 'bent_håla', 'skiklubben'),
(2016, 'bror_?mos', 'skiklubben'),
(2016, 'eina_nygå', 'skiklubben'),
(2016, 'elis_ruud', 'skiklubben'),
(2015, 'espe_egel', 'skiklubben'),
(2016, 'espe_egel', 'skiklubben'),
(2015, 'eva_kvam', 'skiklubben'),
(2016, 'eva_kvam', 'skiklubben'),
(2015, 'frod_mads', 'skiklubben'),
(2016, 'frod_mads', 'skiklubben'),
(2015, 'geir_birk', 'skiklubben'),
(2016, 'geir_birk', 'skiklubben'),
(2016, 'geir_herm', 'skiklubben'),
(2016, 'guri_nord', 'skiklubben'),
(2015, 'hans_løke', 'skiklubben'),
(2016, 'hans_løke', 'skiklubben'),
(2015, 'helg_brei', 'skiklubben'),
(2016, 'helg_brei', 'skiklubben'),
(2015, 'helg_toll', 'skiklubben'),
(2016, 'idar_kals', 'skiklubben'),
(2015, 'ida_mykl', 'skiklubben'),
(2016, 'ida_mykl', 'skiklubben'),
(2015, 'ingr_edva', 'skiklubben'),
(2016, 'ingr_edva', 'skiklubben'),
(2015, 'juli_ande', 'skiklubben'),
(2016, 'juli_ande', 'skiklubben'),
(2015, 'kari_thor', 'skiklubben'),
(2016, 'kari_thor', 'skiklubben'),
(2016, 'kjel_fjel', 'skiklubben'),
(2015, 'kris_even', 'skiklubben'),
(2015, 'kris_hass', 'skiklubben'),
(2016, 'kris_hass', 'skiklubben'),
(2015, 'kris_hass1', 'skiklubben'),
(2015, 'mari_stra', 'skiklubben'),
(2016, 'mari_stra', 'skiklubben'),
(2015, 'mona_lie', 'skiklubben'),
(2016, 'mona_lie', 'skiklubben'),
(2015, 'mort_iver', 'skiklubben'),
(2016, 'mort_iver', 'skiklubben'),
(2015, 'nils_knud', 'skiklubben'),
(2016, 'nils_knud', 'skiklubben'),
(2015, 'odd_moha', 'skiklubben'),
(2015, 'olav_hell', 'skiklubben'),
(2015, 'reid_hamr', 'skiklubben'),
(2016, 'reid_hamr', 'skiklubben'),
(2015, 'rolf_wiik', 'skiklubben'),
(2016, 'rolf_wiik', 'skiklubben'),
(2016, 'sive_nord', 'skiklubben'),
(2015, 'stia_haug', 'skiklubben'),
(2016, 'stia_haug', 'skiklubben'),
(2015, 'terj_mort', 'skiklubben'),
(2016, 'terj_mort', 'skiklubben'),
(2016, 'tom_jako', 'skiklubben'),
(2015, 'tore_svee', 'skiklubben'),
(2015, 'tor_dale', 'skiklubben'),
(2015, 'tron_kris', 'skiklubben'),
(2016, 'tron_kris', 'skiklubben'),
(2015, 'øyst_aase', 'skiklubben'),
(2016, 'øyst_aase', 'skiklubben'),
(2015, 'øyst_lore', 'skiklubben'),
(2016, 'øyst_lore', 'skiklubben'),
(2015, 'øyvi_jens', 'skiklubben'),
(2016, 'øyvi_jens', 'skiklubben'),
(2016, '?rut_?mos', 'vindil'),
(2015, 'cami_erik', 'vindil'),
(2016, 'cami_erik', 'vindil'),
(2015, 'erik_lien', 'vindil'),
(2015, 'erik_pete', 'vindil'),
(2015, 'gunn_berg', 'vindil'),
(2016, 'gunn_berg', 'vindil'),
(2015, 'henr_lore', 'vindil'),
(2016, 'henr_lore', 'vindil'),
(2016, 'idar_kals1', 'vindil'),
(2015, 'mart_halv', 'vindil'),
(2016, 'mart_halv', 'vindil'),
(2016, 'stia_andr', 'vindil'),
(2015, 'stia_henr', 'vindil'),
(2016, 'stia_henr', 'vindil'),
(2015, 'thom_inge', 'vindil'),
(2016, 'thom_inge', 'vindil'),
(2015, 'tom_bråt', 'vindil'),
(2016, 'tom_bråt', 'vindil'),
(2015, 'tom_bøe', 'vindil'),
(2016, 'tom_bøe', 'vindil'),
(2015, 'tron_moen', 'vindil'),
(2016, 'tron_moen', 'vindil'),
(2015, 'øyst_sæth', 'vindil'),
(2016, 'øyst_sæth', 'vindil');

-- --------------------------------------------------------

--
-- Table structure for table `Skier`
--

CREATE TABLE `Skier` (
  `userName` varchar(40) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `yearOfBirth` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Skier`
--

INSERT INTO `Skier` (`userName`, `firstname`, `lastname`, `yearOfBirth`) VALUES
('?hal_?mos', '?Halvor', '?Mostuen', 2009),
('?jan_tang', '?Jan', 'Tangen', 2007),
('?rut_?mos', '?Ruth', '?Mostuen', 2002),
('?rut_nord', '?Ruth', 'Nordli', 2006),
('ande_andr', 'Anders', 'Andresen', 2004),
('ande_rønn', 'Anders', 'Rønning', 2001),
('andr_stee', 'Andreas', 'Steen', 2001),
('anna_næss', 'Anna', 'Næss', 2005),
('arne_anto', 'Arne', 'Antonsen', 2005),
('arne_inge', 'Arne', 'Ingebrigtsen', 2005),
('astr_amun', 'Astrid', 'Amundsen', 2001),
('astr_sven', 'Astrid', 'Svendsen', 2008),
('bent_håla', 'Bente', 'Håland', 2009),
('bent_svee', 'Bente', 'Sveen', 2003),
('beri_hans', 'Berit', 'Hanssen', 2003),
('bjør_aase', 'Bjørn', 'Aasen', 2006),
('bjør_ali', 'Bjørn', 'Ali', 2008),
('bjør_rønn', 'Bjørg', 'Rønningen', 2009),
('bjør_sand', 'Bjørn', 'Sandvik', 1997),
('bror_?mos', 'Bror', '?Mostuen', 2005),
('bror_kals', 'Bror', 'Kalstad', 2006),
('cami_erik', 'Camilla', 'Eriksen', 2005),
('dani_hamm', 'Daniel', 'Hammer', 2000),
('eina_nygå', 'Einar', 'Nygård', 2009),
('elis_ruud', 'Elisabeth', 'Ruud', 2003),
('elle_wiik', 'Ellen', 'Wiik', 2004),
('erik_haal', 'Erik', 'Haaland', 2007),
('erik_lien', 'Erik', 'Lien', 2008),
('erik_pete', 'Erik', 'Petersen', 2002),
('espe_egel', 'Espen', 'Egeland', 2005),
('espe_haal', 'Espen', 'Haaland', 2004),
('eva_kvam', 'Eva', 'Kvam', 2000),
('fred_lien', 'Fredrik', 'Lien', 2000),
('frod_mads', 'Frode', 'Madsen', 2008),
('frod_rønn', 'Frode', 'Rønningen', 2005),
('geir_birk', 'Geir', 'Birkeland', 2010),
('geir_herm', 'Geir', 'Hermansen', 2003),
('gerd_svee', 'Gerd', 'Sveen', 2001),
('gunn_berg', 'Gunnar', 'Berge', 2009),
('guri_nord', 'Guri', 'Nordli', 2003),
('hann_stei', 'Hanno', 'Steiro', 2005),
('hans_foss', 'Hans', 'Foss', 1998),
('hans_løke', 'Hans', 'Løken', 2005),
('hara_bakk', 'Harald', 'Bakken', 2002),
('heid_dani', 'Heidi', 'Danielsen', 2005),
('helg_brei', 'Helge', 'Breivik', 2006),
('helg_toll', 'Helge', 'Tollefsen', 2003),
('henr_bern', 'Henrik', 'Berntsen', 2003),
('henr_dale', 'Henrik', 'Dalen', 2005),
('henr_lore', 'Henrik', 'Lorentzen', 2006),
('hild_hass', 'Hilde', 'Hassan', 2007),
('håko_jens', 'Håkon', 'Jensen', 2005),
('idar_kals', 'Idar', 'Kalstad', 2007),
('idar_kals1', 'Idar', 'Kalstad', 2002),
('ida_mykl', 'Ida', 'Myklebust', 2001),
('inge_simo', 'Inger', 'Simonsen', 2004),
('inge_thor', 'Inger', 'Thorsen', 2006),
('ingr_edva', 'Ingrid', 'Edvardsen', 2001),
('juli_ande', 'Julie', 'Andersson', 2003),
('kari_thor', 'Karin', 'Thorsen', 2002),
('kjel_fjel', 'Kjell', 'Fjeld', 2004),
('knut_bye', 'Knut', 'Bye', 2006),
('kris_even', 'Kristian', 'Evensen', 2004),
('kris_hass', 'Kristin', 'Hassan', 2003),
('kris_hass1', 'Kristian', 'Hassan', 2004),
('lind_lore', 'Linda', 'Lorentzen', 2004),
('liv_khan', 'Liv', 'Khan', 2006),
('magn_sand', 'Magnus', 'Sande', 2003),
('mari_bye', 'Marit', 'Bye', 2003),
('mari_dahl', 'Marit', 'Dahl', 2004),
('mari_eile', 'Marius', 'Eilertsen', 2000),
('mari_stra', 'Marius', 'Strand', 2005),
('mart_halv', 'Martin', 'Halvorsen', 2002),
('mona_lie', 'Mona', 'Lie', 2004),
('mort_iver', 'Morten', 'Iversen', 2003),
('nils_bakk', 'Nils', 'Bakke', 2003),
('nils_knud', 'Nils', 'Knudsen', 2006),
('odd_moha', 'Odd', 'Mohamed', 2005),
('olav_bråt', 'Olav', 'Bråthen', 2000),
('olav_eike', 'Olav', 'Eikeland', 2008),
('olav_hell', 'Olav', 'Helle', 2007),
('olav_lien', 'Olav', 'Lien', 2002),
('ole_borg', 'Ole', 'Borge', 2002),
('reid_hamr', 'Reidun', 'Hamre', 2008),
('rolf_wiik', 'Rolf', 'Wiik', 2002),
('rune_haga', 'Rune', 'Haga', 2005),
('sara_okst', 'Sarah', 'Okstad', 2003),
('silj_mykl', 'Silje', 'Myklebust', 2007),
('sive_nord', 'Sivert', 'Nordli', 2009),
('solv_solb', 'Solveig', 'Solbakken', 2004),
('stia_andr', 'Stian', 'Andreassen', 2004),
('stia_haug', 'Stian', 'Haugland', 2002),
('stia_henr', 'Stian', 'Henriksen', 2001),
('terj_mort', 'Terje', 'Mortensen', 2003),
('thom_inge', 'Thomas', 'Ingebrigtsen', 2006),
('tom_bråt', 'Tom', 'Bråthen', 2008),
('tom_bøe', 'Tom', 'Bøe', 2008),
('tom_jako', 'Tom', 'Jakobsen', 2002),
('tore_gulb', 'Tore', 'Gulbrandsen', 2005),
('tore_svee', 'Tore', 'Sveen', 2001),
('tor_dale', 'Tor', 'Dalen', 2005),
('tove_moe', 'Tove', 'Moe', 2002),
('trin_kals', 'Trine', 'Kalstad', 2009),
('tron_kris', 'Trond', 'Kristensen', 2006),
('tron_moen', 'Trond', 'Moen', 2004),
('øyst_aase', 'Øystein', 'Aasen', 2007),
('øyst_lore', 'Øystein', 'Lorentzen', 2004),
('øyst_sæth', 'Øystein', 'Sæther', 2000),
('øyvi_hell', 'Øyvind', 'Helle', 2000),
('øyvi_jens', 'Øyvind', 'Jenssen', 1999),
('øyvi_kvam', 'Øyvind', 'Kvam', 2000),
('øyvi_vike', 'Øyvind', 'Viken', 2004);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Club`
--
ALTER TABLE `Club`
  ADD PRIMARY KEY (`id`),
  ADD KEY `city` (`city`);

--
-- Indexes for table `ClubCity`
--
ALTER TABLE `ClubCity`
  ADD PRIMARY KEY (`city`);

--
-- Indexes for table `Logs`
--
ALTER TABLE `Logs`
  ADD PRIMARY KEY (`SuserName`,`year`);

--
-- Indexes for table `Season`
--
ALTER TABLE `Season`
  ADD PRIMARY KEY (`SuserName`,`fallYear`),
  ADD KEY `Cid` (`Cid`);

--
-- Indexes for table `Skier`
--
ALTER TABLE `Skier`
  ADD PRIMARY KEY (`userName`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Club`
--
ALTER TABLE `Club`
  ADD CONSTRAINT `Club_ibfk_1` FOREIGN KEY (`city`) REFERENCES `ClubCity` (`city`) ON UPDATE CASCADE;

--
-- Constraints for table `Logs`
--
ALTER TABLE `Logs`
  ADD CONSTRAINT `Logs_ibfk_1` FOREIGN KEY (`SuserName`) REFERENCES `Skier` (`userName`) ON UPDATE CASCADE;

--
-- Constraints for table `Season`
--
ALTER TABLE `Season`
  ADD CONSTRAINT `Season_ibfk_1` FOREIGN KEY (`SuserName`) REFERENCES `Skier` (`userName`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Season_ibfk_2` FOREIGN KEY (`Cid`) REFERENCES `Club` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
