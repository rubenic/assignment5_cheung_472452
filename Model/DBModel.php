<?php


class DBModel{

  protected $db = null;

  public function __construct($db = null){
    if($db){
      $this->db = $db;
    }
    else{
      $this->db = new PDO('mysql:host=localhost;dbname=Assignment5;charset=utf8mb4','root','');
    }
  }

  //EXPORT THE SKIER TABLE
  public function exportSkierToDB($ski){

    $stmt = $this->db->prepare("INSERT INTO Skier (userName,firstname,lastname,yearOfBirth) VALUES (?,?,?,?)");
    $stmt->execute(array($ski->userName,$ski->fn,$ski->ln,$ski->yearOfBirth));
  }

  //EXPORT THE CLUB AND CITY TABLE
  public function exportClubToDB($club){

    $stmt1 = $this->db->prepare("INSERT INTO ClubCity (city,county) VALUES (?,?)");
    $stmt1->execute(array($club->city,$club->county));

    //EXPORT THE CLUB TABLE
    $stmt2 = $this->db->prepare("INSERT INTO Club (id,clubName,city) VALUES (?,?,?)");
    $stmt2->execute(array($club->id,$club->clubName,$club->city));
  }


  //EXPORT THE SEASON AND LOG TABLE
  public function exportSeasonAndLogToDB($season){
    $stmt3 = $this->db->prepare("INSERT INTO Season (fallYear, SuserName, Cid) VALUES (?,?,?)");
    $stmt3->execute(array($season->fallYear,$season->SuserName,$season->Cid));

    $stmt4 = $this->db->prepare("INSERT INTO Logs (totalDistance,SuserName,year) VALUES (?,?,?)");
    $stmt4->execute(array($season->distance,$season->SuserName,$season->fallYear));
  }

}

?>
