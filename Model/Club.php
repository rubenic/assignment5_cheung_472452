<?php


class Club{
  public $id;
  public $clubName;
  public $city;
  public $county;

// Constructor

  public function __construct($id, $clubName, $city, $county){
    $this->id = $id;
    $this->clubName = $clubName;
    $this->city = $city;
    $this->county = $county;
  }
}
?>
