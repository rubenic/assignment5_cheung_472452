<?php

include_once("Model/Skier.php");
include_once("Model/Club.php");

class XMLModel{
  protected $dom = null;

  public function __construct(){

    $this->dom = new DOMDocument();
    $this->dom->load('SkierLogs.xml');
  }


  public function getSkierList(){

// gets the skiers from xml file
    $xpath = new DOMXpath($this->dom);
    $skierlist = array();

        $elements = $xpath->query("/SkierLogs/Skiers/Skier");
        foreach ($elements as $skieryo){

          $valueUN = $skieryo->getAttribute('userName');
          $xmlfirstname = $skieryo->getElementsByTagName('FirstName');
          $valueFN = $xmlfirstname->item(0)->textContent;
          $xmllastname = $skieryo->getElementsByTagName('LastName');
          $valueLN = $xmllastname->item(0)->textContent;
          $xmlyearofbirth = $skieryo->getElementsByTagName('YearOfBirth');
          $valueYOB = $xmlyearofbirth->item(0)->nodeValue;


          $skierlist[] = new Skier($valueUN,$valueFN,$valueLN,$valueYOB);
        }

          return $skierlist;
  }

  public function getClubList(){
    $xpath = new DOMXpath($this->dom);
    $clublist = array();

    $elements = $xpath->query('/SkierLogs/Clubs/Club');

    foreach($elements as $club){
      $valueID = $club->getAttribute('id');
      $xmlname = $club->getElementsByTagName('Name');
      $valueNAME = $xmlname->item(0)->textContent;
      $xmlcity = $club->getElementsByTagName('City');
      $valueCITY = $xmlcity->item(0)->textContent;
      $xmlcounty = $club->getElementsByTagName('County');
      $valueCOUNTY = $xmlcounty->item(0)->textContent;

      $clublist[] = new Club($valueID,$valueNAME,$valueCITY,$valueCOUNTY);


    }
      return $clublist;
  }


  public function getSeasons(){
    $xpath = new DOMXpath($this->dom);
    $seasonlist = array();


    $seasons = $xpath->query('/SkierLogs/Season');

    foreach($seasons as $season){
      $year = $season->getAttribute('fallYear');
      $skierclubs = $season->getElementsByTagName('Skiers');
      foreach($skierclubs as $clubs){
        $clubid = $clubs->getAttribute('clubId');
        $skiers = $clubs->getElementsByTagName('Skier');
        foreach($skiers as $skier){
          $skiername = $skier->getAttribute('userName');
          $entries = $skier->getElementsByTagName('Entry');
          $totdistance = 0;
          foreach($entries as $entry){
            $distances = $entry->getElementsByTagName('Distance');
            $totdistance += $distances->item(0)->nodeValue;

          }
          $seasonlist[] = new Season($year,$clubid,$skiername,$totdistance);
        }
      }
    }
    return $seasonlist;

  }

}

?>
