<?php

include_once("Model/XMLModel.php");
include_once("Model/DBModel.php");
include_once("Model/Season.php");
include_once("View/View.php");


class Controller{
  protected $doc;
  protected $dbm;

  public function __construct(){

    session_start();
        $this->doc = new XMLModel();
        $this->dbm = new DBModel();
    }

  public function invoke(){

    //SKIERS TO DB
    $skiers = array();
    $skiers = $this->doc->getSkierList();

    foreach($skiers as $element){
      $this->dbm->exportSkierToDB($element);
    }

    //CLUBS TO DB
    $clubs = array();
    $clubs = $this->doc->getClubList();

    foreach($clubs as $club){
      $this->dbm->exportClubToDB($club);
    }

    //SEASON TO DB
    $seasons = array();
    $seasons = $this->doc->getSeasons();

    foreach($seasons as $season){
      $this->dbm->exportSeasonAndLogToDB($season);
    }

    $view = new View();
    $view->create();
  }

}



?>
