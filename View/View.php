<?php
/** The View is the superclass that sets up the page for each of the views.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */

Class View {



	public function create() {
	    echo <<<HTML
<!DOCTYPE html>
<html>
<head>
<title>
HTML;
		echo <<<HTML
</title>
</head>

<body>
<h1>
HTML;
        echo "Loading from XML file 'SkierLogs.xml'...";
		echo <<<HTML
</h1>
HTML;
        echo "Loading complete! - There were no errors detected";
		echo <<<HTML
</body>
</html>
HTML;
		 }
	}
?>
